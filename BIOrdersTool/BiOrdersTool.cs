﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bdx.BIReporting.Data;
using LinqToExcel;
using Nhs.Utility.Common;
using Bdx.BIReporting.Common.Business;
using System.Diagnostics;

namespace BIOrdersTool
{
    public partial class BiOrdersTool : Form
    {
        private static Dictionary<string, WorkOrderBuilderComparison> orderList = null;
        private static object Locker = new object();

        public BiOrdersTool()
        {
            InitializeComponent();
        }
        

        private void BiOrdersTool_Load(object sender, EventArgs e)
        {
            // BQProcessor.ExecuteQuery(BQProcessor.WorkOrdersQuery, BQProcessor.BQKey, new [] {"", ""});

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // https://www.microsoft.com/en-us/download/confirmation.aspx?id=23734
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                button1.Enabled = false;
                progressBar1.Maximum = 100;
                progressBar1.Step = 1;
                progressBar1.Value = 0;

                dataGridView1.DataSource = null;
                textBox1.Text = openFileDialog1.FileName;
                backgroundWorker1.WorkerReportsProgress = true;
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var watch = new Stopwatch();

            watch.Start();

            try
            {
                orderList = new Dictionary<string, WorkOrderBuilderComparison>();
                var excelFile = new ExcelQueryFactory(textBox1.Text);

                backgroundWorker1.ReportProgress(1, "Loading Excel Data...");

                var orderMappingData = (from c in excelFile.Worksheet<WorkOrderBuilder>() select c).ToList();

                backgroundWorker1.ReportProgress(10, "Loading Big Query Data...");

                var ordersResult = BQProcessor.ExecuteQuery(BQProcessor.WorkOrdersQuery, BQProcessor.BQKey, null);
                var orders = BQResultMapper<WorkOrderBuilder>.MapBqResult(ordersResult);

                backgroundWorker1.ReportProgress(30, "Comparing Data (30%)...");

                double progress = 30;

                foreach (var order in orderMappingData)
                {
                    double step = (double) 70 / (double) orderMappingData.Count;

                    var tableOrder = orders.FirstOrDefault(o =>
                        o.Order.Trim() == order.Order.Trim() && o.BuilderId != order.BuilderId);
                    if (tableOrder != null && !orderList.Keys.Contains(order.Order))
                    {
                        orderList.Add(order.Order.Trim(),
                            new WorkOrderBuilderComparison()
                            {
                                Order = order.Order.Trim(),
                                BuilderIdFile = order.BuilderId,
                                BuilderIdTable = tableOrder.BuilderId
                            });
                    }
                    progress += step;
                    backgroundWorker1.ReportProgress((int) progress, "Comparing Data (" + (int) progress + "%)...");
                }
            }
            catch (Exception ex)
            {
                backgroundWorker1.ReportProgress(100, "Error! " + ex.Message);
                return;
            }

            watch.Stop();
            backgroundWorker1.ReportProgress(100, "Done! Time Elapsed: " + (watch.ElapsedMilliseconds / 1000) + " secs");            
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Visible = true;
            progressBar1.Value = e.ProgressPercentage;
            label1.Text = e.UserState.ToString();
            if (e.ProgressPercentage == 100)
            {
                if (e.UserState.ToString().Contains("Error"))
                {
                    label1.Text = e.UserState.ToString();
                    label1.ForeColor = Color.Red;
                }
                else
                {
                    label1.ForeColor = Color.LimeGreen;
                    dataGridView1.AutoGenerateColumns = false;
                    dataGridView1.DataSource = orderList.Select(o => o.Value).ToList();
                    button1.Enabled = true;
                }
            }
        }
    }
}
