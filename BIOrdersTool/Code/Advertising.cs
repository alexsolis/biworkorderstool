﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bdx.BIReporting.Data
{

    public class WorkOrder
    {
        public string Order { get; set; }
    }

    public class WorkOrderBuilderComparison
    {
        public string Order { get; set; }
        public int BuilderIdFile { get; set; }
        public int BuilderIdTable { get; set; }

    }

    public class WorkOrderBuilder
    {
        [ExcelColumn("WorkOrder")]
        public string Order { get; set; }
        [ExcelColumn("BDXID")]
        public int BuilderId { get; set; }

    }


    public class Advertising
    {
        [ExcelColumn("Month")]
        public int Month { get; set; }
        [ExcelColumn("ProductGroup")]
        public string ProductGroup { get; set; }
        [ExcelColumn("Sales Rep")]
        public string SalesRep { get; set; }
        [ExcelColumn("Adv. ID")]
        public string AdvId { get; set; }
        [ExcelColumn("Brand")]
        public string Brand { get; set; }
        [ExcelColumn("AO")]
        public string Order { get; set; }
        [ExcelColumn("Market")]
        public string Market { get; set; }
        [ExcelColumn("CPM Rate")]
        public decimal RateCpm { get; set; }
        [ExcelColumn("Flat Rate")]
        public decimal RateFlat { get; set; }
        [ExcelColumn("Imps. Sched")]
        public decimal ImpSched { get; set; }
        [ExcelColumn("Imps. Deliv")] 
        public decimal ImpDeliv { get; set; }
        [ExcelColumn("% of   Total")]
        public decimal TotalPerc { get; set; }
        [ExcelColumn("Clicks")]
        public int Clicks { get; set; }
        [ExcelColumn("CTR")]
        public decimal Ctr { get; set; }
        [ExcelColumn("Rate")]
        public decimal Rate { get; set; }
    }
}
