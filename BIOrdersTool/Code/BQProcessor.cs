﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Bdx.BIReporting.Common.Business;
using Google.Apis.Bigquery.v2;
using Google.Apis.Bigquery.v2.Data;
using Google.Apis.Services;
using Google.Apis.Auth.OAuth2;
using System.Diagnostics;

namespace Bdx.BIReporting.Data
{
    public static class BQProcessor
    {
        public static string GoogleProjectName
        {
            get { return "bdx-dw-prod"; }
        }
    

    public static string GoogleBQAppName
    {
        get
        {
            return "BI Reports";
        }
    }

    public static string GoogleBQDatasetName
    {
        get
        {
            return "Reports";
        }
    }
        


    public static string BQKey
        {
            get
            {
                return @"{
                          type: 'service_account',
                          project_id: 'bdx-dw-prod',
                          private_key_id: 'aa69dc5da35baebc8a9ed114dd6e34359d5bc8ab',
                          private_key: '-----BEGIN PRIVATE KEY----- MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDNK/DHHV/DLUEn nT6MsW0HsntwyFGMom19MCyLq3W48OI9O4Ykmcc+IPE1JHmAxbCKCTsbUMbTliuI 7I87mqCLZdbEs6DKPOBPVxADs+swFT7z4ViKawKXYmdjS4Yjos0NnADXiJkgbACj KKZWFtB6iJAgnKEN6oi4kPPhhypVYal9CKlmLtNQMi/StjwjGcIbgp3pKAIWjSAL 6tkQ+HCeY4qyhDwlGvpiZMRWiZXFXisj8bicA8XR+Ts4MnUQe+N6buXEM2cDgAC9 gZpMpmZqJ6hjpUTWFftr4XQVsDGsxaNimR/WiEqmYiCR9PrZiwiQyscL8ZFrORqF c6m8o/jZAgMBAAECggEBAJOnF7R0E0KnAsi7hRRDa6+qZuOXhfirvBfn32b0kdfy ox5mA/e7I4Q2uOABy988ZfwxAHR8AVqsK9ByTfIbKYaoV5Nfokto3BS9Fy1TAeKs p41jkxCqSkcf7j+rTnG+68rrhj+Wcny+vQaubmZuLMLCHNcbYVxeeN46kwiu+WxP feHW3juKOKx0Z188N1nJAYnaV/g0d8GRAnCrH0f03xC2e/xZqepHHrx5cahsQ6iR GFgygImZNYp5HKw+l2xgxL5h9uoEC0FlcG/4eNmZMciK2e39t47VU4W6qKR/ZTtU 2p0fNO4AcHS3N2wVmEPaZb+SzsHO4lSOYC01+uvjwjECgYEA9ATDEWHZ0nO/xnGL Zk/C8tWiMXOeJXIHAFg5rLH7Jsbs0RXyv3LIku7fREU64AN159IVAyqEqgrdMWSx b0HMOEXC8Nh/Xt5NY1bTiqwwT+VLqlV1Vo3mX6xwq5URtM1k2ErKfvOVeFDD/LQK CPCSLVy07bN93tz4mjk3sLkE4N8CgYEA1z7iZMlW6a4yFNvW4njzyYwdKZh92wv0 BvwzLpGkNimXAZdp6p9A673RMlSUtRRzQUALFJqsd+xJcRQfD/PEs3uAZX6sg0Nl BNk/FthxrxN6hCQTBj+a6lk2CnHa5sQvG18N6AG6W8MGDOvwFrQA2+5Sx8yfAtkF vvWYAcaCxUcCgYAjTwkW28zT+fG2+jkMjeAMmUMF2s/4ZR6EOz/lMD/CJaigvIdK koZZVH0z+LyDK50E9U4bDizyGe5VtldCXQf64IVLqlgL1yjlhevINhArj2c6d3Eq HBBIqcAH4+MTAWFlNrVEV2S6ax1g1IBbOMw+ChsYBg/hjqHeVwcq6Cq/iwKBgFqG tRqkXyxaue9x4jdPnWB30FANNWlp5pEFgtUkJcctyLe/oakun1RGefRMBFsAJdUc hUx+HOk29FBdoQ7JfEHsQDzvgMiYEKVwZQtLrsxuScnHjkTDfXjwWj0cU29YtWMy oBuilmk/ZY0Lsecqf24AFsq3MW02Lyd4DR06CwfDAoGBANXmiOeTxEKifvaJCJW6 rDXTRw+7TTHo9Pb2aRo/ZU5rS9M5BLSeuCgxTWXXoe/ECxae30/1wylle4f4cC7n 3oAVPThuLHZD31257vKi+jhdYvJv1O8UUMDrAGeYNydWLL+SSE0KNEIYUf71yri/ Yao566v5+xtw8L860CNHCW2z -----END PRIVATE KEY----- ',
                          client_email: 'bq-query-service-acct@bdx-dw-prod.iam.gserviceaccount.com',
                          client_id: '103207106306983188055',
                          auth_uri: 'https://accounts.google.com/o/oauth2/auth',
                          token_uri: 'https://accounts.google.com/o/oauth2/token',
                          auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
                          client_x509_cert_url: 'https://www.googleapis.com/robot/v1/metadata/x509/bq-query-service-acct%40bdx-dw-prod.iam.gserviceaccount.com'
                        }";
            }
        }

        public static Query WorkOrdersQuery
        {
            get
            {
                var query = new Query();
                query.Id = "WorkOrdersBQ";
                query.QueryText = @"Select advt_order_id as Order, bdx_id as BuilderId 
                                    FROM Reports.crm_workorder_bdx_id
                                    Group by Order, BuilderId";

                return query;
            }
        }

        public static BQResult ExecuteQuery(Query query, string bigQueryKey, object[] array = null)
        {
            var uploadStream = new MemoryStream(Encoding.UTF8.GetBytes(bigQueryKey));

            GoogleCredential credentials = GoogleCredential.FromStream(uploadStream);

            if (credentials.IsCreateScopedRequired)
                credentials = credentials.CreateScoped(new[] { BigqueryService.Scope.Bigquery });

            string projectId = GoogleProjectName;

            var serviceInitializer = new BaseClientService.Initializer()
            {
                ApplicationName = GoogleBQAppName,
                HttpClientInitializer = credentials
            };

            var bqService = new BigqueryService(serviceInitializer);

            if (query.QueryText == null)
                return new BQResult();

            Job job = new Job();
            JobConfigurationQuery jobConfigQuery = new JobConfigurationQuery();
            jobConfigQuery.Query = array != null ? string.Format(query.QueryText, array) : query.QueryText;
            jobConfigQuery.UseQueryCache = query.UseQueryCache;
            jobConfigQuery.DefaultDataset = new DatasetReference { ProjectId = projectId, DatasetId = GoogleBQDatasetName };

            JobConfiguration jobConfig = new JobConfiguration();
            jobConfig.Query = jobConfigQuery;
            jobConfig.DryRun = query.DryRunQuery;
            job.Configuration = jobConfig;
            job.Id = Guid.NewGuid().ToString();

            JobsResource.InsertRequest request = bqService.Jobs.Insert(job, projectId);
            Job jobResult = request.Execute() ;

            if (query.DryRunQuery)
            {
                return new BQResult();
            }

            Job jobStatus = bqService.Jobs.Get(projectId, jobResult.JobReference.JobId).Execute();

            string jStatus = jobStatus.Status.State;
            while (jStatus.ToLower() == "running")
            {
                if (query.TimeoutMilliSeconds > 0)
                    System.Threading.Thread.Sleep(500); // Do not sleep all the time out try to get the results each 500ms
                jobStatus = bqService.Jobs.Get(projectId, jobResult.JobReference.JobId).Execute();
                jStatus = jobStatus.Status.State;
            }

            GetQueryResultsResponse queryResult;
            BQResult bqResult = new BQResult();

            List<TableRow> tableResult = new List<TableRow>();
            var resultRequest = new JobsResource.GetQueryResultsRequest(bqService, projectId, jobResult.JobReference.JobId)
            {
                MaxResults = 500000
            };

            queryResult = resultRequest.Execute();

            bqResult.TotalRows = queryResult.TotalRows;
            bqResult.Schema = queryResult.Schema;
            if (queryResult.Rows != null)
                tableResult.AddRange(queryResult.Rows);

            //Get the remaining rows in case of large results
            if (queryResult.TotalRows != null && Convert.ToInt32(queryResult.TotalRows) > tableResult.Count)
            {
                while (Convert.ToInt32(queryResult.TotalRows) > tableResult.Count)
                {
                    resultRequest.PageToken = queryResult.PageToken;
                    queryResult = resultRequest.Execute();
                    tableResult.AddRange(queryResult.Rows);
                }
            }
            bqResult.Rows = tableResult;

            return bqResult;
        }
    }
}
