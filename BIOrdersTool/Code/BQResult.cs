﻿using System.Collections.Generic;
using System.Linq;
using Google.Apis.Bigquery.v2.Data;

namespace Bdx.BIReporting.Common.Business
{
    public class BQResult
    {
        public ulong? TotalRows { get; set; }
        public TableSchema Schema { get; set; }
        public IList<TableRow> Rows { get; set; }

        /// <summary>
        /// Full outer Join of BQResults using as comparison the first Column of each BQResult
        /// </summary>
        /// <param name="aResult"></param>
        /// <param name="bResult"></param>
        /// <returns></returns>
        public static BQResult JoingBqResults(BQResult aResult, BQResult bResult)
        {
            var result = new BQResult();

            // Merge the columns
            var columns = aResult.Schema.Fields.ToList();
            columns.AddRange(bResult.Schema.Fields.Skip(1));

            result.Schema = new TableSchema() { Fields = columns };

            // First step get the total different month of the two results
            List<string> months = new List<string>();
            IEnumerable<string> monthsFromB = new List<string>();
            int numberOfCellsFromA = aResult.Schema.Fields.Count - 1;//Minus the first column
            int numberOfCellsFromB = bResult.Schema.Fields.Count - 1;//Minus the first column
            if (aResult.Rows != null)
            {
                months = aResult.Rows.Select(r => r.F.FirstOrDefault().V.ToString()).ToList();
            }
            if (bResult.Rows != null)
            {
                monthsFromB = bResult.Rows.Select(r => r.F.FirstOrDefault().V.ToString());
            }
            
            months.AddRange(monthsFromB);

            var distictMonths = months.Distinct();

            var resultRows = new List<TableRow>();

            // Iterate through the month and take the values of the different cells for each row of the tables
            foreach (var month in distictMonths)
            {
                TableRow rowFromA = null;
                TableRow rowFromB = null;

                if (aResult.Rows != null)
                {
                    rowFromA = aResult.Rows.FirstOrDefault(r => r.F.FirstOrDefault().V.ToString().Equals(month));
                }

                if (bResult.Rows != null)
                {
                    rowFromB = bResult.Rows.FirstOrDefault(r => r.F.FirstOrDefault().V.ToString().Equals(month));
                }

                // Add the cells to the row
                var row = new TableRow();
                var cells = new List<TableCell> { new TableCell() { V = month } };

                if (rowFromA != null)
                {
                    // Skip the month cell since I already add it
                    AddCellToList(cells, rowFromA.F, 1);

                    if (rowFromB != null)
                    {
                        AddCellToList(cells, rowFromB.F, 1);
                    }
                    else
                    {
                        AddDefaultCellToList(cells, numberOfCellsFromB);
                    }
                }
                else
                {
                    AddDefaultCellToList(cells, numberOfCellsFromA);

                    if (rowFromB != null)
                    {
                        AddCellToList(cells, rowFromB.F, 1);
                    }
                    else
                    {
                        // This case should not happen since I assured that we have month from one result or another
                        AddDefaultCellToList(cells, numberOfCellsFromB);
                    }
                }

                row.F = cells;

                resultRows.Add(row);
            }

            result.Rows = resultRows;

            return result;
        }

        private static void AddCellToList(IList<TableCell> targetList, IList<TableCell> sourceList, int skipIndex = 0)
        {
            for (int i = skipIndex; i < sourceList.Count; i++)
            {
                targetList.Add(sourceList[i]);
            }
        }

        private static void AddDefaultCellToList(IList<TableCell> listOfCells, int quantity)
        {
            for (int i = 0; i < quantity; i++)
            {
                listOfCells.Add(new TableCell() { V = 0 });
            }
        }
    }
}
