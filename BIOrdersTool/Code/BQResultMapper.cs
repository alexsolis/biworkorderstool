﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.Apis.Bigquery.v2.Data;

namespace Bdx.BIReporting.Common.Business
{
    public class BQResultMapper<T> where T : class, new()
    {

        /// <summary>
        /// Tries to map all properties of the class based on the comlum names of the query
        /// </summary>
        /// <param name="result">BQResult data</param>
        /// <returns>A list of class with mapped values</returns>
        public static List<T> MapBqResult(BQResult result)
        {
                List<T> resultList = new List<T>();
                if (result?.Rows == null || result.Rows.Count == 0)
                    return resultList;

                var props = typeof(T).GetProperties();
                foreach (TableRow t in result.Rows)
                {
                    var newT = new T();
                    for (int indexSchema = 0; indexSchema < result.Rows[0].F.Count; indexSchema++)
                    {
                        var name = result.Schema.Fields[indexSchema].Name;
                        var value = t.F[indexSchema].V;
                        var prop = props.FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase));
                        if (prop == null) continue;
                        if (value != null)
                        {
                            if (Nullable.GetUnderlyingType(prop.PropertyType) != null)
                                prop.SetValue(newT, Convert.ChangeType(value, Nullable.GetUnderlyingType(prop.PropertyType)), null);
                            else
                                prop.SetValue(newT, Convert.ChangeType(value, prop.PropertyType), null);

                        }
                    }
                    resultList.Add(newT);
                }
                
                return resultList;            
        }

        /// <summary>
        /// Tries to map all properties of the class based on the comlum names of the query
        /// </summary>
        /// <param name="result">BQResult data</param>
        /// <param name="classValues">A class to set the found values</param>
        /// <returns>A class with mapped values</returns>
        public static T MapBqResult(BQResult result, T classValues)
        {
            if (result == null || result.Rows == null || result.Rows.Count == 0) return classValues;
            var props = typeof(T).GetProperties();
            for (int index = 0; index < result.Rows[0].F.Count; index++)
            {
                var name = result.Schema.Fields[index].Name;
                var value = result.Rows[0].F[index].V;

                var prop = props.FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase));
                if (prop == null) continue;
                if (value != null)
                {
                    if (Nullable.GetUnderlyingType(prop.PropertyType) != null)
                        prop.SetValue(classValues, Convert.ChangeType(value, Nullable.GetUnderlyingType(prop.PropertyType)), null);
                    else
                        prop.SetValue(classValues, Convert.ChangeType(value, prop.PropertyType), null);
                }
            }

            return classValues;
        }
    }

    public static class BQResultMapperHelper
    {

        public static T ToMapBqResult<T>(this BQResult result) where T : class, new()
        {
            var classValues = new T();
            if (result?.Rows == null || result.Rows.Count == 0) return classValues;

            var props = typeof(T).GetProperties();

            for (var index = 0; index < result.Rows[0].F.Count; index++)
            {
                var name = result.Schema.Fields[index].Name;
                var value = result.Rows[0].F[index].V;

                var prop = props.FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase));
                if (prop == null) continue;
                if (value != null)
                {
                    if (Nullable.GetUnderlyingType(prop.PropertyType) != null)
                        prop.SetValue(classValues, Convert.ChangeType(value, Nullable.GetUnderlyingType(prop.PropertyType)), null);
                    else
                        prop.SetValue(classValues, Convert.ChangeType(value, prop.PropertyType), null);
                }
            }

            return classValues;
        }
        /// <summary>
        /// Tries to map all properties of the class based on the comlum names of the query
        /// </summary>
        /// <param name="result">BQResult data</param>
        /// <returns>A list of class with mapped values</returns>
        public static List<Dictionary<string, object>> MapBqResults(this BQResult result)
        {
            List<Dictionary<string, object>> resultsMapped = new List<Dictionary<string, object>>();
            if (result == null || result.Rows == null) return new List<Dictionary<string, object>>();

            foreach (TableRow t in result.Rows)
            {
                var newRow = new Dictionary<string, object>();
                for (int indexSchema = 0; indexSchema < result.Rows[0].F.Count; indexSchema++)
                {
                    newRow.Add(result.Schema.Fields[indexSchema].Name, t.F[indexSchema].V);
                }
                resultsMapped.Add(newRow);
            }

            return resultsMapped;
        }

        public static Dictionary<string, object> MapBqResult(this BQResult result)
        {
            Dictionary<string, object> resultsMapped = new Dictionary<string, object>();
            if (result == null || result.Rows == null) return new Dictionary<string, object>();
            var row = result.Rows.AsEnumerable().FirstOrDefault();
            if (row != null)
            {
                for (int indexSchema = 0; indexSchema < result.Rows[0].F.Count; indexSchema++)
                {
                    resultsMapped.Add(result.Schema.Fields[indexSchema].Name, row.F[indexSchema].V);
                }
            }
            return resultsMapped;
        }
    }
}