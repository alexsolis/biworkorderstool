﻿using System.Xml.Serialization;

namespace Bdx.BIReporting.Common.Business
{
    [XmlType(AnonymousType = true)]
    public class Query
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("pathToSqlFile")]
        public string PathToSqlFile { get; set; }

        [XmlAttribute("cacheQueryOnBQ")]
        public bool CacheQueryOnBQ { get; set; }

        [XmlAttribute("useQueryCache")]
        public bool UseQueryCache { get; set; }

        [XmlAttribute("dryRunQuery")]
        public bool DryRunQuery { get; set; }

        [XmlAttribute("timeoutMilliSeconds")]
        public int TimeoutMilliSeconds { get; set; }

        public string QueryText { get; set; }
    }
}
